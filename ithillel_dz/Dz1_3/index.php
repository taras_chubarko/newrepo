<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.07.2021
 * Time: 10:48
 */

class ValueObject
{
    private $red;
    private $green;
    private $blue;

    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct(int $red, int $green, int $blue)
    {
        $this->setRed($red);
        $this->setGreen($green);
        $this->setBlue($blue);
    }

    /* public function getRed
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRed(): int
    {
        return $this->red;
    }

    /* public function getGreen
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getGreen(): int
    {
        return $this->green;
    }

    /* public function getBlue
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getBlue(): int
    {
        return $this->blue;
    }

    /* public function setRed
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setRed(int $red)
    {
        if ($red < 0 || $red > 255) {
            throw new InvalidArgumentException('$red должно быть от 0 до 255');
        }
        $this->red = $red;
    }

    /* public function setGreen
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setGreen(int $green)
    {
        if ($green < 0 || $green > 255) {
            throw new InvalidArgumentException('$green должно быть от 0 до 255');
        }
        $this->green = $green;
    }

    /* public function setBlue
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setBlue(int $blue)
    {
        if ($blue < 0 || $blue > 255) {
            throw new InvalidArgumentException('$blue должно быть от 0 до 255');
        }
        $this->blue = $blue;
    }

    /* public function equals
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function equals(object $obj): bool
    {
        if ($this == $obj) {
            return true;
        }
        return false;
    }

    /* public function random
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function rdm(): object
    {
        $this->setRed(random_int(0,255));
        $this->setGreen(random_int(0,255));
        $this->setBlue(random_int(0,255));
        return $this;
    }

    /* public function rdm
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function random(ValueObject $object)
    {
        return $object->rdm();
    }

    /* public function mix
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function mix(object $obj): object
    {
        $this->setRed($this->averageValue([$this->red, $obj->red]));
        $this->setGreen($this->averageValue([$this->green, $obj->green]));
        $this->setBlue($this->averageValue([$this->blue, $obj->blue]));
        return $this;
    }

    /* public function averageValue
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function averageValue(array $arr): int
    {
        if (count($arr)) {
            return (int)array_sum($arr) / count($arr);
        }
        return 0;
    }


}

$obj = new ValueObject(10, 250, 255);
$red = $obj->getRed();
$green = $obj->getGreen();
$blue = $obj->getBlue();

echo "$red, $green, $blue </br>";
//Реализовать метод equals, который будет сравнивать объекты цветов и возвращать true или false.
$equals = $obj->equals(new ValueObject(10, 30, 255));
echo ($equals) ? 'true </br>' : 'false </br>';
//Реализовать статический метод random, который будет возвращать объект RGB цвета с случайными значениями свойств red, green, blue.
$random = ValueObject::random($obj);
$red = $random->getRed();
$green = $random->getGreen();
$blue = $random->getBlue();
echo "$red, $green, $blue </br>";
//Реализовать метод mix, который будет цвета. Смешивать (вычислять среднее число для каждого свойства цвета).
$color = new ValueObject(200, 200, 200);
$mixedColor = $color->mix(new ValueObject(100, 100, 100));
$red = $mixedColor->getRed();
$green = $mixedColor->getGreen();
$blue = $mixedColor->getBlue();
echo "$red, $green, $blue </br>";