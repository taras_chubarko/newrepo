<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 11:46
 */
require_once __DIR__ . '/vendor/autoload.php';

use App\Classes\Contact;

$contact = new Contact();

$newContact = $contact->set()
    ->phone('000-555-000')
    ->name("John")
    ->surname("Surname")
    ->email("john@email.com")
    ->address("Some address")
    ->build();


echo sprintf('<pre>%s</pre>', print_r($newContact, true));

$newContact2 = $contact->set()
    ->phone('222-555-000')
    //->name("John2")
    ->surname("Surname2")
    ->email("john@email.com")
    ->address("Some address 2")
    ->build();

echo sprintf('<pre>%s</pre>', print_r($newContact2, true));