<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 11:56
 */

namespace App\Classes;

class Contact implements IContact
{
    protected $result;

    protected function reset()
    {
        $this->result = new \stdClass();
    }

    public function set(): IContact
    {
        $this->reset();
        return $this;
    }

    public function phone(string $phone): IContact
    {
        $this->result->phone = $phone;
        return $this;
    }

    public function name(string $name): IContact
    {
        $this->result->name = $name;
        return $this;
    }

    public function surname(string $surname): IContact
    {
        $this->result->surname = $surname;
        return $this;
    }

    public function email(string $email): IContact
    {
        $this->result->email = $email;
        return $this;
    }

    public function address(string $address): IContact
    {
        $this->result->address = $address;
        return $this;
    }

    public function build(): object
    {
       return $this->result;
    }
}