<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 11:51
 */

namespace App\Classes;


interface IContact
{
    public function set(): IContact;

    public function phone(string $phone): IContact;

    public function name(string $name): IContact;

    public function surname(string $surname): IContact;

    public function email(string $email): IContact;

    public function address(string $address): IContact;

    public function build(): object;
}