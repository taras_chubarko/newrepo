<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.07.2021
 * Time: 12:43
 */


abstract class Model
{
    protected $id;

    protected $stores = [];

    /* public function load
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function find(int $id)
    {
        $stores = unserialize($_SESSION['stores']);
        $result = array_search($id, array_column($stores, 'id'));
        if($result !== false){
            return $stores[$result];
        }
        return null;
    }

    /* public function create
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function create(array $arr)
    {
        // INSERT INTO user (id, name, email) VALUES (:id, :name, :email)
        if ($_SESSION['stores']) {
            $this->stores = unserialize($_SESSION['stores']);
            $arr['id'] = count($this->stores) + 1;
            $this->id = $arr['id'];
            $this->stores[] = $arr;
            $_SESSION['stores'] = serialize($this->stores);
        } else {
            $arr['id'] = 1;
            $this->id = $arr['id'];
            $this->stores[] = $arr;
            $_SESSION['stores'] = serialize($this->stores);
        }

        return $arr;
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update(array $arr, int $id = null)
    {
        // UPDATE user SET name = :name, email = 'email' WHERE id = :id
        $stores = unserialize($_SESSION['stores']);
        $result = array_search($id, array_column($stores, 'id'));
        if($result !== false){
            $arr['id'] = $id;
            $stores[$result] = $arr;
            $_SESSION['stores'] = serialize($stores);
            return true;
        }

        return false;
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete()
    {
        // DELETE user WHERE id = :id

        $stores = unserialize($_SESSION['stores']);
        $result = array_search($this->id, array_column($stores, 'id'));
        if($result !== false){
            array_splice($stores, $result);
            $_SESSION['stores'] = serialize($stores);
            return true;
        }
        return false;
    }

    /* public function read
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function read()
    {
        if ($_SESSION['stores']) {
            if (!is_null($this->id)) {
                $this->stores = unserialize($_SESSION['stores']);
                $result = array_search($this->id, array_column($this->stores, 'id'));
                if($result !== false){
                    return $this->stores[$result];
                }
                return null;
            }
        } else {
            $_SESSION['stores'] = '';
        }
        return null;
    }

    /* public function save
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function save()
    {
        $model = $this->read();
        $vars = get_object_vars($this);
        unset($vars['stores']);
        //$_SESSION['stores'] = '';
       if($model){
          $result =  $this->update($vars, $this->id);
       }else{
           $result = $this->create($vars);
       }
        //echo sprintf('<pre>%s</pre>', print_r($result, true));
        return $result;
    }

}

class User extends Model
{
    public $id;
    public $name;
    public $email;
}
session_start();

$user = User::find(1);
echo sprintf('<pre>%s</pre>', print_r($user, true));
//* несовсем ясно как реализовать такую конструкцию через статический метод
//$user->name = 'taras';
//$result = $user->save();
//* несовсем ясно как реализовать такую конструкцию через статический метод
//$result = $user->delete();


//Create a new user;
$model = new  User();
$model->name = 'John';
$model->email = 'some@gmail.com';
$result = $model->save();
echo sprintf('<pre>%s</pre>', print_r($result, true));



