<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 10:38
 */
require_once __DIR__.'/vendor/autoload.php';

$factoryMethod = new \App\Classes\Delivery();
clientCode($factoryMethod);

function clientCode($factoryMethod){
    $car = $factoryMethod->makeDelivery('econom');
    setString($car->getModel());
    setString('$'.$car->getPrice());
    //
    $car = $factoryMethod->makeDelivery('standart');
    setString($car->getModel());
    setString('$'.$car->getPrice());
    //
    $car = $factoryMethod->makeDelivery('lux');
    setString($car->getModel());
    setString('$'.$car->getPrice());
}

function setString($line_in) {
    echo $line_in."<br/>";
}