<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 06.08.2021
 * Time: 15:22
 */

class Logger
{
    private $source;

    public function __construct(LoggerInterface $source)
    {
        $this->source = $source;
    }

    public function log($string)
    {
        $this->source->deliver($this->source->format($string));
    }

}

interface LoggerInterface
{
    public function format($string);

    public function deliver($format);
}

class  MyLog implements  LoggerInterface
{
    private $format;
    private $delivery;

    public function __construct($format, $delivery)
    {
        $this->format   = $format;
        $this->delivery = $delivery;
    }

    public function format($string)
    {
        switch ($this->format) {
            case 'raw' :
                {
                    return $string;
                }
                break;
            case 'with_date':
                {
                    return date('Y-m-d H:i:s') . $string;
                }
                break;
            case 'with_date_and_details':
                {
                    return date('Y-m-d H:i:s') . $string . ' - With some details';
                }
                break;
            default:
                {
                    die('Error format');
                }
        }
    }

    public function deliver($format)
    {
        switch ($this->delivery) {
            case 'by_email' :
                {
                    echo "Вывод формата ({$format}) по имейл";
                }
                break;
            case 'by_sms':
                {
                    echo "Вывод формата ({$format}) в смс";
                }
                break;
            case 'to_console':
                {
                    echo "Вывод формата ({$format}) в консоль";
                }
                break;
            default:
                {
                    die('Error deliver');
                }
        }
    }
}
$myLog = new MyLog('raw', 'by_sms');
$logger = new Logger($myLog);
$logger->log('Emergency error! Please fix me!');