<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 15:15
 */
require_once __DIR__ . '/vendor/autoload.php';

use App\Classes\Routing\Route;

$router = new Route();

require_once __DIR__ . '/routes/route.php';

$router->dispatch();