<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 16.08.2021
 * Time: 15:38
 */

namespace App\Classes\Routing;


class Route implements IRouting
{
    protected $routes = [];

    protected $params = [];

    protected $host_prefix = '/ithillel_dz/Dz8_15';

    protected $method;

    protected $controllerNamespace = 'App\\Controllers';

    public function add(string $route, string $ca)
    {
//        $route = preg_replace("/\//", "\\/", $route);
//        $route = preg_replace("/\{([a-z]+)\}/", "(?P<\1>[a-z-]+)", $route);
//
//        $route = preg_replace("/\{([a-z]+):([^\}])\}/", "(?P<\1>\2)", $route);
//
//        $route = "/^{$route}$/i";
        $this->routes[$route] = $ca;

        return $this;
    }

    /* public function getRoutes
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /* public function getParams
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /* public function match
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function match(string $uri): bool
    {
        foreach ($this->routes as $route => $ca) {
            $pattern = preg_replace('/\/{(.*?)}/', '/(.*?)', $route);
            if (preg_match_all('#^' . $pattern . '$#', $uri, $matches, PREG_OFFSET_CAPTURE)) {
                $ca = explode('@', $ca);

                $matches = array_slice($matches, 1);

                $params = array_map(function ($match, $index) use ($matches) {
                    if (isset($matches[$index + 1]) && isset($matches[$index + 1][0]) && is_array($matches[$index + 1][0])) {
                        if ($matches[$index + 1][0][1] > -1) {
                            return trim(substr($match[0][0], 0, $matches[$index + 1][0][1] - $match[0][1]), '/');
                        }
                    }
                    return isset($match[0][0]) && $match[0][1] != -1 ? trim($match[0][0], '/') : null;
                }, $matches, array_keys($matches));

                $this->params = [
                    'controller' => $ca[0],
                    'action' => $ca[1],
                    'params' => $params,
                ];
                return true;
            }
        }
        return false;
    }

    public function dispatch()
    {
        $uri = $this->getURI();
        if ($this->match($uri)) {
            $controller = $this->controllerNamespace . '\\' . $this->params['controller'];
            if (class_exists($controller)) {
                $controller = new $controller();
                call_user_func_array(array($controller, $this->params['action']), $this->params['params']);
            }else {
                throw new \Exception('Controller not found.');
            }
        } else {
            throw new \Exception('404 not found', 404);
        }

    }

    /* public function request_uri
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    private function getURI(): string
    {
        return trim(str_replace($this->host_prefix, '', $_SERVER['REQUEST_URI']), '/');
    }

    /* public function method
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function method($method)
    {
        $this->method = strtoupper($method);
        return $this;
    }
}