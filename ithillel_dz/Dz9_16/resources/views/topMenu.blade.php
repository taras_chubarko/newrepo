<ul class="nav pt-2">
    <li class="nav-item">
        <a class="nav-link" aria-current="page" href="{{getenv('HOST')}}/">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{getenv('HOST')}}/articles">Articles</a>
    </li>
    @if(!\Core\Help::authUser())
        <li class="nav-item">
            <a class="nav-link" href="{{getenv('HOST')}}/login">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{getenv('HOST')}}/register">Register</a>
        </li>
    @else
        @if(\Core\Help::authUser()->login === 'admin')
            <li class="nav-item">
                <a class="nav-link" href="{{getenv('HOST')}}/admin">Control Panel</a>
            </li>
        @endif
        <li class="nav-item">
            <a class="nav-link" href="{{getenv('HOST')}}/logout">Logout</a>
        </li>
    @endif
</ul>