@extends('main')

@section('content')
    <div class="p-5">
        <div class="post-one">
            <h1>{{$post->title}}</h1>
            <time>{{\Carbon\Carbon::parse($post->created_at)->format('d.m.Y в H:i')}}</time>
            {!! $post->body !!}
        </div>
    </div>
@endsection