@extends('main')

@section('content')
    <div class="p-5">
        @foreach($posts as $post)
            <div class="post">
                <h2><a href="{{getenv('HOST')}}/article/{{$post->id}}">{{$post->title}}</a></h2>
                <time>{{\Carbon\Carbon::parse($post->created_at)->format('d.m.Y в H:i')}}</time>
                <p>{{$post->word_shortener($post->body, 20)}}</p>
            </div>
        @endforeach
    </div>
@endsection