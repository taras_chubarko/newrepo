@if(!empty($route))
    <a href="{{$route}}" class="btn btn-outline-info btn-sm me-2"><i class="fas fa-pencil-alt"></i></a>
@endif