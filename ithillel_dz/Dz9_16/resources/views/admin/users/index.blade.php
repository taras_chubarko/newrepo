@extends('admin.main')

@section('content')
    <div class="p-5">
        <a href="{{getenv('HOST')}}/admin/users/create" class="btn btn-primary btn-sm">Add</a>
        <table class="table">
            <thead>
            <tr>
                <th width="20">#</th>
                <th>Login</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Date</th>
                <th align="center" width="100">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($users as $user)
                <tr>
                    <td>{{$user->id}}</td>
                    <td>{{$user->login}}</td>
                    <td>{{$user->first_name}}</td>
                    <td>{{$user->last_name}}</td>
                    <td>{{$user->created_at}}</td>
                    <td align="center" class="d-flex">
                        @include('admin.button_edit', ['route' => getenv('HOST').'/admin/users/'.$user->id.'/edit'])
                        @include('admin.button_delete', ['route' => getenv('HOST').'/admin/users/'.$user->id])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection