@if($post)
    <form class="post-form" method="post" action="{{getenv('HOST')}}/admin/articles/{{$post->id}}/edit">
@else
    <form class="post-form" method="post" action="{{getenv('HOST')}}/admin/articles/create">
@endif
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Title</label>
        <input name="title" type="text" value="{{$post ? $post->title: ''}}" class="form-control"
               id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Body</label>
        <textarea name="body" class="form-control">
                {!! $post ? $post->body: '' !!}
        </textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
