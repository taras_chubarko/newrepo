@extends('admin.main')

@section('content')
    <div class="p-5">
        <a href="{{getenv('HOST')}}/admin/articles/create" class="btn btn-primary btn-sm">Add</a>
        <table class="table">
            <thead>
            <tr>
                <th width="20">#</th>
                <th>Title</th>
                <th>Date</th>
                <th align="center">Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($posts as $post)
                <tr>
                    <td>{{$post->id}}</td>
                    <td><a href="{{getenv('HOST')}}/article/{{$post->id}}" target="_blank">{{$post->title}}</a></td>
                    <td>{{$post->updated_at}}</td>
                    <td align="center" class="d-flex">
                        @include('admin.button_edit', ['route' => getenv('HOST').'/admin/articles/'.$post->id.'/edit'])
                        @include('admin.button_delete', ['route' => getenv('HOST').'/admin/articles/'.$post->id])
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection