@extends('admin.main')

@section('content')
    <div class="p-5">
        @include('admin.posts.form')
    </div>
@endsection