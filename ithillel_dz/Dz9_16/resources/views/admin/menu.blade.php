<ul class="nav pt-2">
    <li class="nav-item">
        <a class="nav-link" aria-current="page" href="{{getenv('HOST')}}/">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{getenv('HOST')}}/admin/articles">Articles</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{getenv('HOST')}}/admin/users">Users</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="{{getenv('HOST')}}/logout">Logout</a>
    </li>
</ul>