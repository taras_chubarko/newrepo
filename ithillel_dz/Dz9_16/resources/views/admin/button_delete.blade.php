@if(!empty($route))
    <form method="post" action="{{$route}}">
        <button type="submit" class="btn btn-outline-danger btn-sm"><i class="far fa-trash-alt"></i></button>
    </form>
@endif