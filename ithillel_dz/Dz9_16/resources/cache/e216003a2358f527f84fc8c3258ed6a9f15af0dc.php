

<?php $__env->startSection('content'); ?>
    <div class="p-5">
        <div class="post-one">
            <h1><?php echo e($post->title); ?></h1>
            <time><?php echo e(\Carbon\Carbon::parse($post->created_at)->format('d.m.Y в H:i')); ?></time>
            <?php echo $post->body; ?>

        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/posts/article.blade.php ENDPATH**/ ?>