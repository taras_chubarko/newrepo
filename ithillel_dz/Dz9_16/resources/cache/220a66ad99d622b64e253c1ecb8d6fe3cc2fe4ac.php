

<?php $__env->startSection('content'); ?>
    <div class="p-5">
        <a href="<?php echo e(getenv('HOST')); ?>/admin/users/create" class="btn btn-primary btn-sm">Add</a>
        <table class="table">
            <thead>
            <tr>
                <th width="20">#</th>
                <th>Login</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Date</th>
                <th align="center" width="100">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($user->id); ?></td>
                    <td><?php echo e($user->login); ?></td>
                    <td><?php echo e($user->first_name); ?></td>
                    <td><?php echo e($user->last_name); ?></td>
                    <td><?php echo e($user->created_at); ?></td>
                    <td align="center" class="d-flex">
                        <?php echo $__env->make('admin.button_edit', ['route' => getenv('HOST').'/admin/users/'.$user->id.'/edit'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php echo $__env->make('admin.button_delete', ['route' => getenv('HOST').'/admin/users/'.$user->id], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/admin/users/index.blade.php ENDPATH**/ ?>