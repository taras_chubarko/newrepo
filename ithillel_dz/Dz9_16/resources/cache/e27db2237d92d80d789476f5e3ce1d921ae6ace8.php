<ul class="nav pt-2">
    <li class="nav-item">
        <a class="nav-link" aria-current="page" href="<?php echo e(getenv('HOST')); ?>/">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/admin/articles">Articles</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/admin/users">Users</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/logout">Logout</a>
    </li>
</ul><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/admin/menu.blade.php ENDPATH**/ ?>