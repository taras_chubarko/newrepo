<ul class="nav pt-2">
    <li class="nav-item">
        <a class="nav-link" aria-current="page" href="<?php echo e(getenv('HOST')); ?>/">Home</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/articles">Articles</a>
    </li>
    <?php if(!\Core\Help::authUser()): ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/login">Login</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/register">Register</a>
        </li>
    <?php else: ?>
        <?php if(\Core\Help::authUser()->login === 'admin'): ?>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/admin">Control Panel</a>
            </li>
        <?php endif; ?>
        <li class="nav-item">
            <a class="nav-link" href="<?php echo e(getenv('HOST')); ?>/logout">Logout</a>
        </li>
    <?php endif; ?>
</ul><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/topMenu.blade.php ENDPATH**/ ?>