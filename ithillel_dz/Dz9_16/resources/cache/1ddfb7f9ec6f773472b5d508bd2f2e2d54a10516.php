

<?php $__env->startSection('content'); ?>
    <div class="p-5">
        <a href="<?php echo e(getenv('HOST')); ?>/admin/articles/create" class="btn btn-primary btn-sm">Add</a>
        <table class="table">
            <thead>
            <tr>
                <th width="20">#</th>
                <th>Title</th>
                <th>Date</th>
                <th align="center">Actions</th>
            </tr>
            </thead>
            <tbody>
            <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <tr>
                    <td><?php echo e($post->id); ?></td>
                    <td><a href="<?php echo e(getenv('HOST')); ?>/article/<?php echo e($post->id); ?>" target="_blank"><?php echo e($post->title); ?></a></td>
                    <td><?php echo e($post->updated_at); ?></td>
                    <td align="center" class="d-flex">
                        <?php echo $__env->make('admin.button_edit', ['route' => getenv('HOST').'/admin/articles/'.$post->id.'/edit'], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                        <?php echo $__env->make('admin.button_delete', ['route' => getenv('HOST').'/admin/articles/'.$post->id], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                    </td>
                </tr>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
            </tbody>
        </table>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('admin.main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/admin/posts/index.blade.php ENDPATH**/ ?>