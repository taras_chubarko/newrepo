<?php if($_SESSION['msg']): ?>
    <div id="msg" class="alert alert-<?php echo e($_SESSION['msg']['type']); ?> mt-2" role="alert">
        <?php if(is_array($_SESSION['msg']['msg'])): ?>
            <?php $__currentLoopData = $_SESSION['msg']['msg']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <p><?php echo $item; ?></p>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php else: ?>
            <p><?php echo e($_SESSION['msg']['msg']); ?></p>
        <?php endif; ?>
    </div>
    <script>
        setTimeout(function () {
            $.ajax({
                type: "POST",
                url: "<?php echo e(getenv('HOST')); ?>/clearsession",
                data: {
                    ses: 'msg'
                },
                success: function (result) {
                    $('#msg').remove();
                }
            });
        }, 2000)
    </script>
<?php endif; ?><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/msg.blade.php ENDPATH**/ ?>