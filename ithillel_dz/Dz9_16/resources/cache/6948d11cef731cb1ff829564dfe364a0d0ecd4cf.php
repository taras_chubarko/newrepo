<?php if($post): ?>
    <form class="post-form" method="post" action="<?php echo e(getenv('HOST')); ?>/admin/articles/<?php echo e($post->id); ?>/edit">
<?php else: ?>
    <form class="post-form" method="post" action="<?php echo e(getenv('HOST')); ?>/admin/articles/create">
<?php endif; ?>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Title</label>
        <input name="title" type="text" value="<?php echo e($post ? $post->title: ''); ?>" class="form-control"
               id="exampleInputEmail1" aria-describedby="emailHelp">
    </div>
    <div class="mb-3">
        <label for="exampleInputEmail1" class="form-label">Body</label>
        <textarea name="body" class="form-control">
                <?php echo $post ? $post->body: ''; ?>

        </textarea>
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
<?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/admin/posts/form.blade.php ENDPATH**/ ?>