<?php if(!empty($route)): ?>
    <form method="post" action="<?php echo e($route); ?>">
        <button type="submit" class="btn btn-outline-danger btn-sm"><i class="far fa-trash-alt"></i></button>
    </form>
<?php endif; ?><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/admin/button_delete.blade.php ENDPATH**/ ?>