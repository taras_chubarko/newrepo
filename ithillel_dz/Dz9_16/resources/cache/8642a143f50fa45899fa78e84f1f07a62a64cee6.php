

<?php $__env->startSection('content'); ?>
    <div class="p-5">
        <?php $__currentLoopData = $posts; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $post): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div class="post">
                <h2><a href="<?php echo e(getenv('HOST')); ?>/article/<?php echo e($post->id); ?>"><?php echo e($post->title); ?></a></h2>
                <time><?php echo e(\Carbon\Carbon::parse($post->created_at)->format('d.m.Y в H:i')); ?></time>
                <p><?php echo e($post->word_shortener($post->body, 20)); ?></p>
            </div>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz9_16\resources\views/posts/index.blade.php ENDPATH**/ ?>