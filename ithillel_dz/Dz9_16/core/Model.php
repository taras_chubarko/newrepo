<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:06
 */

namespace Core;

use Carbon\Carbon;
use PDO;
use PDOException;

class Model
{
    protected static $table = null;

    protected static $select;

    protected $conditions = [];

    public static function q()
    {
        return new static();
    }

    protected static function db()
    {
        static $db = null;

        $host = getenv('DB_HOST'); // сервер баз данных
        $db_name = getenv('DB_NAME');    // имя базы данных
        $charset = "utf8";     // кодировка базы данных
        $user = getenv('DB_USER');     // имя пользователя
        $password = getenv('DB_PASSWORD');    // пароль

        $opt = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);

        if ($db === null) {
            $dsn = "mysql:host=$host;dbname=$db_name;charset=$charset";

            try {
                $dbh = new PDO($dsn, $user, $password, $opt);
                return $dbh;
            } catch (PDOException $e) {
                die('Подключение не удалось: ' . $e->getMessage());
            }
        }

        return $db;
    }

    public static function create($arr = [])
    {
        $arr['created_at'] = Carbon::now();
        $arr['updated_at'] = Carbon::now();

        $f = self::fields($arr);

        if (!is_null(static::$table)) {
            $query = static::db()->prepare("INSERT INTO " . static::$table . " ({$f['fields_string']}) VALUES ({$f['values']})");
            $query->execute($arr);

        } else {
            throw new \Exception('Table dos not exist');
        }
    }

    protected static function fields($arr = [])
    {
        $keys = array_keys($arr);
        $fields_string = implode(', ', $keys);

        $values = [];

        foreach ($arr as $k => $value) {
            $values[] = ':' . $k;
        }

        return [
            'fields_string' => $fields_string,
            'values' => implode(', ', $values),
        ];
    }

    /* public function tableExists
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function tableExists(): bool
    {
        try {
            $result = static::db()->query("SELECT id FROM " . static::$table . " LIMIT 1");
        } catch (PDOException $e) {
            // We got an exception == table not found
            return FALSE;
        }
        return $result !== FALSE;
    }

    public static function find($id = null)
    {
        return self::q()->where('id', '=', $id)->first();
    }

    /* public function __toString
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __toString()
    {
        $string = "SELECT ";

        $string .= "*";

        $string .= " FROM " . static::$table;

        if ($this->conditions) {
            $string .= " WHERE (" . implode(") AND (", $this->conditions) . ")";
        }

        return $string;
    }

    /* public function where
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function where($column, $operator = null, $value = null)
    {
        $value = is_int($value) ? $value : "'$value'";
        $this->conditions[] = $column . $operator . $value;
        return $this;
    }

    /* public function execute
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    protected function execute($string = null)
    {
        try {
            if (self::tableExists()) {
                $string = $string ? $string : $this->__toString();
                //dd($string);
                $query = static::db()->prepare($string);
                $query->setFetchMode(PDO::FETCH_CLASS, static::class);
                $query->execute();
                return $query;
            }
            return null;
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
    }

    /* public function get
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get()
    {
        return $this->execute()->fetchAll() ? $this->execute()->fetchAll() : [];
    }

    /* public function first
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function first()
    {
        return $this->execute()->fetch() ? $this->execute()->fetch() : null;
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete(): bool
    {
        try {
            if ($this->id) {
                $id = $this->id;
            } else {
                $id = $this->first()->id;
            }
            if ($id) {
                $this->execute("DELETE FROM " . static::$table . " WHERE id=$id");
                return true;
            }
            return false;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /* public function update
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function update($data = [])
    {
        try {
            if ($this->id) {
                $id = $this->id;
            } else {
                $id = $this->first()->id;
            }
            if ($id) {
                // $this->execute("DELETE FROM " . static::$table . " WHERE id=$id");
                $vals = [];
                foreach ($data as $k => $datum) {
                    $vals[] = "$k='$datum'";
                }
                $f = implode(', ', $vals);
                $this->execute("UPDATE " . static::$table . " SET " . $f . " WHERE id=$id");
                return true;
            }
            return false;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /* public function save
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function save()
    {
        try {

            $f = (array)$this;
            unset($f["\x00*\x00conditions"]);
            unset($f['id']);

            if ($this->id) {
                $id = $this->id;
                unset($f['created_at']);
                //unset($f['updated_at']);

                $vals = [];
                foreach ($f as $k => $datum) {
                    if ($k == 'updated_at') {
                        $vals[] = "$k='" . Carbon::now() . "'";
                    } else {
                        $vals[] = "$k='$datum'";
                    }
                }
                $f = implode(', ', $vals);
                $this->execute("UPDATE " . static::$table . " SET " . $f . " WHERE id=$id");
            } else {
                self::create($f);
            }
            return $this;
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /* public function count
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function count()
    {
        return $this->execute()->fetchAll() ? count($this->execute()->fetchAll()) : 0;
    }

    /* public function word_shortener
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function word_shortener($text, $words = 10, $sp = '...')
    {
        $all = explode(' ', $text);
        $str = '';
        $count = 1;

        foreach ($all as $key) {
            $str .= $key . ($count >= $words ? '' : ' ');
            $count++;
            if ($count > $words) {
                break;
            }
        }

        return $str . (count($all) <= $words ? '' : $sp);
    }

    /* public function toArray
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function toArray()
    {
        $arr = (array)$this;
        unset($arr["\x00*\x00conditions"]);
        return $arr;
    }


}