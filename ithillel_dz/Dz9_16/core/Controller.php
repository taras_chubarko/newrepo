<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:06
 */

namespace Core;

use Jenssegers\Blade\Blade;

class Controller
{
    /* public function render
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function view($view, $params = [])
    {
        $views = new Blade('resources/views', 'resources/cache');
        return $views->make($view, $params);
    }


    /* public function validate
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function validate(): bool
    {
        if ($_REQUEST) {
            $msg = [];
            foreach ($_REQUEST as $k => $value) {
                if ($value === '') {
                    $msg[] = "<b>$k</b> is required to fill";
                }
            }

            if(!empty($_REQUEST['password']) && !empty($_REQUEST['password_confirmation'])){
                if($_REQUEST['password'] !== $_REQUEST['password_confirmation']){
                    $msg[] = "Password mismatch";
                }
            }

            if (!empty($msg)) {
                $this->msg($msg, 'danger');
            }
            return true;
        }
        return false;
    }

    /* public function msg
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function msg($msg, $type = 'danger', $redirect = null)
    {
        Help::msg($msg, $type, $redirect);
    }

    /* public function before
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function before(): bool
    {
        return true;
    }

    /* public function after
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function after(): void
    {

    }

    /* public function __call
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __call(string $method, array $arguments)
    {
        if (method_exists($this, $method)) {
            if ($this->before() !== false) {
                call_user_func_array([$this, $method], $arguments);
                $this->after();
            }else{
                throw new \Exception('');
            }
        } else {
            throw new \Exception('Method not allowed');
        }
    }

}