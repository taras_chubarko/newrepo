<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 27.08.2021
 * Time: 9:47
 */

namespace Core;


class Help
{
    /* public function msg
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function msg($msg, $type = 'primary', $redirect = null)
    {
        $_SESSION['msg'] = [
            'type' => $type,
            'msg' => $msg,
        ];
        if($redirect){
            header('Location: ' . $_SERVER['HOST'].$redirect);
        }else{
            header('Location: ' . $_SERVER['REDIRECT_URL']);
        }

    }

    /* public function authUser
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function authUser()
    {
        if(!empty($_SESSION['user'])){
            return unserialize($_SESSION['user']);
        }
        return false;
    }
}