<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 27.08.2021
 * Time: 12:11
 */

namespace App\Controllers;


use App\Models\Post;
use Core\Controller;

class AdminPostController extends Controller
{

    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        $posts = Post::q()->get();
        echo $this->view('admin.posts.index')->with(['posts' => $posts])->render();
    }

    /* public function create
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function create()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            echo $this->view('admin.posts.create')->render();
        }
        Post::create($_REQUEST);
        $this->msg('Created!', 'success', '/admin/articles');
        return true;
    }

    /* public function edit
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function edit(int $id)
    {
        $post = Post::find($id);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            echo $this->view('admin.posts.edit')->with(['post' => $post])->render();
        }
        else{
            $post->title = $_REQUEST['title'];
            $post->body = $_REQUEST['body'];
            $post->save();
            $this->msg('Updated!', 'success', '/admin/articles');
            return true;
        }
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete(int $id)
    {
        Post::q()->where('id', '=', $id)->delete();
        $this->msg('Deleted!', 'success', '/admin/articles');
        return true;
    }
}