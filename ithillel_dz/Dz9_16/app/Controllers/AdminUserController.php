<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 28.08.2021
 * Time: 11:53
 */

namespace App\Controllers;


use App\Models\User;
use Core\Controller;

class AdminUserController extends Controller
{
    /* public function index
         * @param
         *-----------------------------------
         *|
         *-----------------------------------
         */
    public function index()
    {
        $users = User::q()->get();
        echo $this->view('admin.users.index')->with(['users' => $users])->render();
    }

    /* public function create
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function create()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            echo $this->view('admin.users.create')->render();
        }
        //User::create($_REQUEST);
        $this->msg('Created!', 'success', '/admin/users');
        return true;
    }

    /* public function edit
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function edit(int $id)
    {
        $user = User::find($id);
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            echo $this->view('admin.users.edit')->with(['user' => $user])->render();
        }
        else{
           // $post->title = $_REQUEST['title'];
           // $post->body = $_REQUEST['body'];
          //  $post->save();
            $this->msg('Updated!', 'success', '/admin/users');
            return true;
        }
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete(int $id)
    {
        if($id !== 1){
            User::q()->where('id', '=', $id)->delete();
        }
        $this->msg('Deleted!', 'success', '/admin/users');
        return true;
    }
}