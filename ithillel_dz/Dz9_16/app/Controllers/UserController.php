<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 27.08.2021
 * Time: 8:52
 */

namespace App\Controllers;


use App\Models\User;
use Core\Controller;

class UserController extends Controller
{
    /* public function login
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function login()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            echo $this->view('users.page_login')->render();
        }

        if ($this->validate()) {
            $user = User::q()->where('login', '=', $_REQUEST['login'])->first();
            if ($user) {
                if (password_verify($_REQUEST['password'], $user->password)) {
                    $_SESSION['user'] = serialize($user);
                    $this->msg('Welcome!', 'success', '/');
                } else {
                    $this->msg('Bad password!');
                }
            } else {
                $this->msg('No User in DB', 'danger', '/register');
            }
        }

    }

    /* public function register
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function register()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            echo $this->view('users.page_register')->render();
        }

        if ($this->validate()) {
            $user = User::q()->where('login', '=', $_REQUEST['login'])->first();
            if(!$user){
                User::create([
                    'login' => $_REQUEST['login'],
                    'first_name' => $_REQUEST['first_name'],
                    'last_name' => $_REQUEST['last_name'],
                    'password' => password_hash($_REQUEST['password'], PASSWORD_BCRYPT),
                ]);
                $this->msg('You have successfully registered', 'success', '/login');
            }else {
                $this->msg('User exists', 'danger', '/register');
            }
        }
    }

    /* public function logout
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function logout()
    {
        unset($_SESSION['user']);
        header('Location: '.getenv('HOST'));
    }
}