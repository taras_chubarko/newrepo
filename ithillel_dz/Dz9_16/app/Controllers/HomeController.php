<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:13
 */

namespace App\Controllers;


use App\Models\Post;
use App\Models\User;
use Carbon\Carbon;
use Core\Controller;
use Core\Help;

class HomeController extends Controller
{
    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        echo $this->view('home')->render();
    }

    /* public function install
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function install()
    {
        Post::createTable();
        User::createTable();

        $faker = \Faker\Factory::create();

        if(Post::q()->count() == 0){
            foreach(range(1, 30) as $i){
                Post::create([
                    'title' => $faker->sentence(),
                    'body' => $faker->text(),

                ]);
            }
        }

        if(User::q()->count() == 0){
            User::create([
                'login' => 'admin',
                'first_name' => 'Taras',
                'last_name' => 'Chubarko',
                'password' => password_hash(123456789, PASSWORD_BCRYPT),
            ]);
        }
        $this->msg('Установка прошла успешно.', 'success', getenv('HOST'));
        return true;
    }

    /* public function clearsession
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function clearsession()
    {
        unset($_SESSION[$_POST['ses']]);
        header('Location: '.getenv('HOST'));
    }
}