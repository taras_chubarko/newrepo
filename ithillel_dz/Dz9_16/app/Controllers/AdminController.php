<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 27.08.2021
 * Time: 12:02
 */

namespace App\Controllers;


use Core\Controller;

class AdminController extends Controller
{
    /* public function index
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function index()
    {
        echo $this->view('admin.index')->render();
    }
}