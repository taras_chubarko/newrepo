<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 14:32
 */

namespace App\Models;


use Core\Model;

class Post extends Model
{
    protected static $table = 'posts';

    /* public function createTable
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public static function createTable()
    {
        $query =  static::db()->prepare('CREATE TABLE IF NOT EXISTS posts (
            id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT UNIQUE, 
            title VARCHAR(255) NOT NULL,
            body LONGTEXT NOT NULL,
            created_at TIMESTAMP NOT NULL,
            updated_at TIMESTAMP NOT NULL
            );');

        return $query->execute();
    }
}