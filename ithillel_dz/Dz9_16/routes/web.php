<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 22.08.2021
 * Time: 13:15
 */

$router->setNamespace('\App\Controllers');

//home
$router->get('/', 'HomeController@index');
$router->post('/install', 'HomeController@install');
$router->post('/clearsession', 'HomeController@clearsession');
//posts
$router->get('/articles', 'PostController@index');
$router->get('/article/{id}', 'PostController@getArticle');
//user
$router->get('/login', 'UserController@login');
$router->post('/login', 'UserController@login');
$router->get('/register', 'UserController@register');
$router->post('/register', 'UserController@register');
$router->get('/logout', 'UserController@logout');

$router->mount('/admin', function () use ($router){
    $router->get('/', 'AdminController@index');
    //post
    $router->get('/articles', 'AdminPostController@index');
    $router->get('/articles/{id}/edit', 'AdminPostController@edit');
    $router->post('/articles/{id}/edit', 'AdminPostController@edit');
    $router->get('/articles/create', 'AdminPostController@create');
    $router->post('/articles/create', 'AdminPostController@create');
    $router->post('/articles/{id}', 'AdminPostController@delete');
    //user
    $router->get('/users', 'AdminUserController@index');
    $router->get('/users/{id}/edit', 'AdminUserController@edit');
    $router->post('/users/{id}/edit', 'AdminUserController@edit');
    $router->get('/users/create', 'AdminUserController@create');
    $router->post('/users/create', 'AdminUserController@create');
    $router->post('/users/{id}', 'AdminUserController@delete');
});

$router->set404(function() {
    header('HTTP/1.1 404 Not Found');
});

