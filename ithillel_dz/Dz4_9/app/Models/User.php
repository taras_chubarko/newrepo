<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 03.08.2021
 * Time: 11:33
 */

namespace App\Models;

use Exception;
use PDO;
use PDOException;

class User
{
    public $id;
    public $name;
    public $surname;
    public $age;
    public $email;
    public $phone;

    private $db;

    protected $fields = ['name', 'surname', 'age', 'email', 'phone'];
    protected $required = ['name', 'surname', 'age', 'email'];

    /* public function __construct
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct()
    {
        $this->db = $this->init();
    }

    /* public function init
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    protected function init()
    {
        $host = 'localhost'; // сервер баз данных
        $db_name = 'dz4_9';    // имя базы данных
        $charset = "utf8";     // кодировка базы данных
        $user = 'mysql';     // имя пользователя
        $password = '';    // пароль
        $dsn = "mysql:host=$host;dbname=$db_name;charset=$charset";
// Дополнительные опции
        $opt = array(
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC);
        try {
            $dbh = new PDO($dsn, $user, $password, $opt);
            return $dbh;
        } catch (PDOException $e) {
            die('Подключение не удалось: ' . $e->getMessage());
        }

    }

    /* public function createTable
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function createTable()
    {
        $query = $this->db->prepare('CREATE TABLE IF NOT EXISTS users (
            id INT(10) PRIMARY KEY NOT NULL AUTO_INCREMENT UNIQUE, 
            name VARCHAR(80) NOT NULL,
            surname VARCHAR(100) NOT NULL,
            age INT(3) NOT NULL,
            email VARCHAR(255) NOT NULL,
            phone  VARCHAR(255)
            );');

        return $query->execute();
    }

    /* public function tableExists
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function tableExists(): bool
    {
        try {
            $result = $this->db->query("SELECT id FROM users LIMIT 1");
        } catch (PDOException $e) {
            // We got an exception == table not found
            return FALSE;
        }
        return $result !== FALSE;
    }

    /* public function create
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function create($arr = []): User
    {
        try {

            $fdb = [];

            foreach ($this->fields as $f) {
                $fdb[] = ':' . $f;
                if (!array_key_exists($f, $arr)) {
                    throw new Exception('Поля не соответствуют.');
                }
            }

            foreach ($this->required as $r) {
                if ($arr[$r] == "") {
                    throw new Exception("$r - обязательно для заполнения.");
                }
            }

            $fl = implode(', ', $this->fields);
            $fdbs = implode(', ', $fdb);


            $query = $this->db->prepare("INSERT INTO users ($fl) VALUES ($fdbs)");
            $query->execute($arr);

            $query = $this->db->prepare('SELECT * FROM users ORDER BY id DESC LIMIT 1');
            $query->setFetchMode(PDO::FETCH_CLASS, 'App\Models\User');
            $query->execute();
            $result = $query->fetch();
            return $result;
        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

    /* public function all
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function all(): array
    {
        try {
            if($this->tableExists()){
                $query = $this->db->prepare('SELECT * FROM users');
                $query->setFetchMode(PDO::FETCH_CLASS, 'App\Models\User');
                $query->execute();
                $result = $query->fetchAll();
                return $result;
            }
            return [];
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
    }

    /* public function delete
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function delete(int $id): bool
    {
        try {
            $query = $this->db->prepare("DELETE FROM users WHERE id=:id");
            $query->bindParam(':id', $id);
            return $query->execute();
        } catch (PDOException $e) {
            die($e->getMessage());
        }
    }

    /* public function find
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function find(int $id): User
    {
        try {
            if($this->tableExists()){
                $query = $this->db->prepare('SELECT * FROM users  WHERE id=:id');
                $query->bindParam(':id', $id);
                $query->setFetchMode(PDO::FETCH_CLASS, 'App\Models\User');
                $query->execute();
                $result = $query->fetch();
                if($result) return $result;
                return new User();
            }
            return null;
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage());
        }
    }
}