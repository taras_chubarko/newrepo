<form action="delete_selected.php" method="post">
    <button type="submit" class="btn btn-primary">Удалить выбраные</button>
    <table class="table">
        <thead>
        <tr>
            <th width="20"></th>
            <th>ID</th>
            <th>Имя</th>
            <th width="100">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($users as $k => $user)
            <tr>
                <td>
                    <input class="form-check-input" type="checkbox" name="id[{{$k}}]" value="{{$user->id}}">
                </td>
                <td>
                    <a href="user/?id={{$user->id}}">
                        {{$user->id}}
                    </a>
                </td>
                <td>{{$user->name}}</td>
                <td>
                    @include('delete_user', ['id' => $user->id])
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</form>