

<?php $__env->startSection('content'); ?>

    <?php if(!$if_table): ?>
        <?php echo $__env->make('create_table', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <?php else: ?>
        <div class="alert alert-warning mb-5" role="alert">
            Таблица Users уже создана
        </div>
        <div class="row">
            <div class="col-8">
                <?php echo $__env->make('users_table', ['users' => $users], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
            <div class="col-4">
                <?php echo $__env->make('user_add', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
            </div>
        </div>
    <?php endif; ?>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('main', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz4_9\views/home.blade.php ENDPATH**/ ?>