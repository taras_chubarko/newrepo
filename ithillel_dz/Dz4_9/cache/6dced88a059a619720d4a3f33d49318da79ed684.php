<form action="delete_selected.php" method="post">
    <button type="submit" class="btn btn-primary">Удалить выбраные</button>
    <table class="table">
        <thead>
        <tr>
            <th width="20"></th>
            <th>ID</th>
            <th>Имя</th>
            <th width="100">Action</th>
        </tr>
        </thead>
        <tbody>
        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $k => $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <tr>
                <td>
                    <input class="form-check-input" type="checkbox" name="id[<?php echo e($k); ?>]" value="<?php echo e($user->id); ?>">
                </td>
                <td>
                    <a href="user/?id=<?php echo e($user->id); ?>">
                        <?php echo e($user->id); ?>

                    </a>
                </td>
                <td><?php echo e($user->name); ?></td>
                <td>
                    <?php echo $__env->make('delete_user', ['id' => $user->id], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
                </td>
            </tr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </tbody>
    </table>
</form><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz4_9\views/users_table.blade.php ENDPATH**/ ?>