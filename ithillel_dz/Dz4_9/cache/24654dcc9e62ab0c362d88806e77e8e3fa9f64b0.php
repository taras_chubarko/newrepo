<form action="add_user.php" method="post">
    <div class="mb-3">
        <label for="name" class="form-label">Имя</label>
        <input type="text" class="form-control" name="name">
    </div>
    <div class="mb-3">
        <label for="surname" class="form-label">Фамилия</label>
        <input type="text" class="form-control" name="surname">
    </div>
    <div class="mb-3">
        <label for="age" class="form-label">Возраст</label>
        <input type="number" class="form-control" name="age">
    </div>
    <div class="mb-3">
        <label for="email" class="form-label">Email</label>
        <input type="email" class="form-control" name="email">
    </div>
    <div class="mb-3">
        <label for="phone" class="form-label">Телефон</label>
        <input type="text" class="form-control" name="phone">
    </div>
    <button type="submit" class="btn btn-primary">Создать пользователя</button>
</form><?php /**PATH E:\OpenServer\domains\localhost\ithillel_dz\Dz4_9\views/user_add.blade.php ENDPATH**/ ?>