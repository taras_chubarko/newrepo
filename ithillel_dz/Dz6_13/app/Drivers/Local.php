<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 16:50
 */

namespace App\Drivers;

use App\Classes\Driver;
use App\Classes\Invoice;


class Local extends Driver
{
    protected $invoice;

    protected $settings;

    public function __construct(Invoice $invoice, $settings)
    {
        $this->invoice($invoice);
        $this->settings = (object) $settings;
    }
}