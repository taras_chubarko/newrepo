<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 16:40
 */

namespace App\Classes;

use App\Classes\Invoice;


abstract class Driver implements DriverInterface
{
    protected $invoice;

    abstract public function __construct(Invoice $invoice, $settings);

    /* public function amount
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function amount($amount)
    {
        $this->invoice->amount($amount);
        return $this;
    }

    /* public function invoice
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function invoice(Invoice $invoice)
    {
        $this->invoice = $invoice;
        return $this;
    }

    /* public function getInvoice
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getInvoice()
    {
        return $this->invoice;
    }
}