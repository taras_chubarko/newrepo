<?php
/**
 * Created by PhpStorm.
 * User: tarik
 * Date: 09.08.2021
 * Time: 16:45
 */

namespace App\Classes;


class Invoice
{
    protected $amount = 0;
    protected $driver;

    /* public function amount
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function amount($amount)
    {
        if (!is_int($amount)) {
            throw new \Exception('Amount value should be an integer.');
        }
        $this->amount = $amount;

        return $this;
    }

    /* public function getAmount
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /* public function getDriver
     * @param
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getDriver()
    {
        return $this->driver;
    }
}